package com.oleh.controller;

import com.oleh.model.Model;

public class Controller implements ControllerInt {

    Model model;

    public Controller() {
        model = new Model();
    }

    @Override
    public void serialize() {
        model.serialize();
    }

    @Override
    public void deserialize() {
        model.deserialize();
    }

    @Override
    public void testNormalInputStream() {
        model.testUsualReader();
    }

    @Override
    public void testBufferedInputStream() {
        model.testBufferedReader();
    }

    @Override
    public void readComments(String fileName) {
        model.readComments(fileName);
    }

    @Override
    public String readFromFileWithNIO(String fileName) {
        return model.readFromFileWithNIO(fileName);
    }

    @Override
    public void writeToFileWithNIO(String data, String fileName) {
        model.writeToFileWithNIO(data, fileName);
    }
}
