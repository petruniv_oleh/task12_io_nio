package com.oleh.controller;


public interface ControllerInt {
    void serialize();

    void deserialize();

    void testNormalInputStream();

    void testBufferedInputStream();

    void readComments(String fileName);

    String readFromFileWithNIO(String fileName);

    void writeToFileWithNIO(String data, String fileName);
}
