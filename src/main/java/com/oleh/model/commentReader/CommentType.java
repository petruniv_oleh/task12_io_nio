package com.oleh.model.commentReader;

public enum CommentType {

    INLINE, BLOCK, NO_COMMENTS, CAN_BE_BOTH;
}
