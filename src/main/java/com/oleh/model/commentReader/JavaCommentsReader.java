package com.oleh.model.commentReader;

import java.io.*;

public class JavaCommentsReader {
    private String fileName;
    private String comments;

    public JavaCommentsReader(String fileName) {
        this.fileName = fileName;
    }

    public void readComments() {
        StringBuilder commentsBuilder = new StringBuilder();
        CommentType commentType = CommentType.NO_COMMENTS;
        try (DataInputStream stream = new DataInputStream(new
                BufferedInputStream(new FileInputStream(fileName)))) {

            int read = stream.read();
            while (read != -1) {
                if (read == '/' && commentType == CommentType.NO_COMMENTS) {
                    commentType = CommentType.CAN_BE_BOTH;
                    commentsBuilder.append((char) read);
                } else {
                    if (read == '/' && commentType == CommentType.CAN_BE_BOTH) {
                        commentType = CommentType.INLINE;
                        commentsBuilder.append((char) read);
                    }
                    if (read == '*' && commentType == CommentType.CAN_BE_BOTH) {
                        commentType = CommentType.BLOCK;
                        commentsBuilder.append((char) read);
                    }
                }

                if (commentType == CommentType.INLINE && read != '\n') {
                    commentsBuilder.append((char) read);
                } else if (commentType==CommentType.INLINE && read == '\n') {
                    commentType = CommentType.NO_COMMENTS;
                }

                if (commentType == CommentType.BLOCK) {
                    commentsBuilder.append((char) read);
                    if (read == '/') {
                        commentType = CommentType.NO_COMMENTS;
                    }
                }
                read = stream.read();
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.comments = commentsBuilder.toString();
    }


    public String getComments() {
        readComments();
        return comments;
    }
}
