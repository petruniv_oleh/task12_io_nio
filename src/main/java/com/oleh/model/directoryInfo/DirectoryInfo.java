package com.oleh.model.directoryInfo;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DirectoryInfo {
    List<String> directoryContent;
    String directoryPath;

    public DirectoryInfo(String directoryPath) {
        this.directoryPath = directoryPath;
    }

    public List<String> getList() {
        makeList();
        return directoryContent;
    }

    public void cd() {
        int i = directoryPath.lastIndexOf('\\');
        directoryPath = directoryPath.substring(0, i + 1);
    }

    public void dir(String dirName) {
        directoryPath = directoryPath + '\\' + dirName;
    }

    private void makeList() {
        File file = new File(directoryPath);
        directoryContent = new ArrayList<>();
        List<File> list = new ArrayList<File>(Arrays.asList(file.listFiles()));

        for (File f : list
        ) {
            try {
                BasicFileAttributes basicFileAttributes = Files.readAttributes(Paths.get(f.getPath()), BasicFileAttributes.class);
                StringBuilder stringBuilder = new StringBuilder();
                if (basicFileAttributes.isDirectory()) {
                    stringBuilder.append("Directory: " + f.getName() + "    creation time: " + basicFileAttributes.creationTime() +
                            "    size: " + basicFileAttributes.size());
                    directoryContent.add(stringBuilder.toString());
                } else {
                    stringBuilder.append("File: " + f.getName() + "    creation time: " + basicFileAttributes.creationTime() +
                            "    size: " + basicFileAttributes.size());
                    directoryContent.add(stringBuilder.toString());
                }
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }

}
