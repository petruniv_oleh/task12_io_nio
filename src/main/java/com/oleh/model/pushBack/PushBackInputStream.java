package com.oleh.model.pushBack;


import java.io.IOException;
import java.io.InputStream;


public class PushBackInputStream{
    private byte[] buffer;
    private int readPosition;
    private InputStream inputStream;

    public PushBackInputStream(InputStream inputStream, int bufferSize) {
        this.inputStream = inputStream;
        this.buffer = new byte[bufferSize];
    }

    public int read() throws IOException {
        if (readPosition < buffer.length) {
            return buffer[readPosition++] & 0xff;
        }
        return inputStream.read();
    }

    public void unread(int b){
        if (readPosition!=0){
            buffer[--readPosition] = (byte) b;
        }
        else{
            System.out.println("The buffer is full! Can`t push back!");
        }
    }
}
