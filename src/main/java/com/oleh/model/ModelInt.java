package com.oleh.model;

public interface ModelInt {

    void serialize();

    void deserialize();

    void testUsualReader();

    void testBufferedReader();

    void readComments(String fileName);

    String readFromFileWithNIO(String fileName);

    void writeToFileWithNIO(String data, String fileName);
}
