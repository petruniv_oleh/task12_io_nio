package com.oleh.model.droids;

import java.io.Serializable;

public class Droid implements Serializable {

    private String droidName;
    private int droidNumber;
    private transient int droidPass;

    public Droid(String droidName, int droidNumber, int droidPass) {
        this.droidName = droidName;
        this.droidNumber = droidNumber;
        this.droidPass = droidPass;
    }

    @Override
    public String toString() {

        return "Droid{" +
                "droidName='" + droidName + '\'' +
                ", droidNumber=" + droidNumber +
                ", droidPass=" + droidPass +
                '}';
    }
}
