package com.oleh.model.droids;

import java.io.Serializable;
import java.util.List;

public class Sheep implements Serializable {
    private String sheepName;
    private Droid[] droids;
    private transient int sheepId;

    public Sheep(String sheepName, int id) {
        this.sheepName = sheepName;
        this.sheepId = id;
    }

    public Droid[] getDroids() {
        return droids;
    }

    public void setDroids(Droid[] droids) {
        this.droids = droids;
    }

    public int getSheepId() {
        return sheepId;
    }

    public void setSheepId(int sheepId) {
        this.sheepId = sheepId;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Droid d: droids
             ) {
            stringBuilder.append(d);
        }
        return "Sheep{" +
                "sheepName='" + sheepName + '\'' +
                ", droids=" + stringBuilder.toString() +
                ", sheepId=" + sheepId +
                '}';
    }
}
