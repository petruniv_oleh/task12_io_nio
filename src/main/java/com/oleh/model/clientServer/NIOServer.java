package com.oleh.model.clientServer;


import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NIOServer implements Runnable {

    private boolean running;

    private Selector selector;
    String writeMsg;
    StringBuffer stringBuffer = new StringBuffer();
    SelectionKey selectionKey;

    Map<SocketAddress, String> userNames = new LinkedHashMap<>();

    public NIOServer() {

        running = true;

    }

    public void initializeServer() {
        try {
            selector = Selector.open();
            ServerSocketChannel serverSocketChannel = ServerSocketChannel.open();
            serverSocketChannel.configureBlocking(false);
            serverSocketChannel.socket().bind(new InetSocketAddress(8800));
            selectionKey = serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
            System.out.println("Server started");
        } catch (IOException ex) {
            Logger.getLogger(NIOServer.class.getName()).log(Level.SEVERE,
                    null, ex);
        }
    }

    public static void main(String[] args) {
        NIOServer server = new NIOServer();
        new Thread(server).start();

    }

    public void execute() {
        try {
            while (running) {
                int num = selector.select();
                if (num > 0) {
                    Iterator<SelectionKey> it = selector.selectedKeys()
                            .iterator();
                    while (it.hasNext()) {
                        SelectionKey key = it.next();
                        it.remove();
                        if (!key.isValid())
                            continue;
                        if (key.isAcceptable()) {
                            getConnection(key);
                        } else if (key.isReadable()) {
                            readMessage(key);
                        } else if (key.isValid() && key.isWritable()) {
                            if (writeMsg != null) {
                                writeMessage(key);
                            }

                        } else
                            break;

                    }

                }
                Thread.yield();
            }

        } catch (IOException ex) {
            Logger.getLogger(NIOServer.class.getName()).log(Level.SEVERE,
                    null, ex);
        }
    }

    private void getConnection(SelectionKey key) throws IOException {
        ServerSocketChannel ssc = (ServerSocketChannel) key.channel();
        SocketChannel sc = ssc.accept();
        sc.configureBlocking(false);
        sc.register(selector, SelectionKey.OP_READ);
        System.out.println("build connection :"
                + sc.socket().getRemoteSocketAddress());
        userNames.put(sc.socket().getRemoteSocketAddress(),"User "+userNames.size());
    }

    private void readMessage(SelectionKey key) throws IOException {
        stringBuffer.delete(0, stringBuffer.length());
        SocketChannel sc = (SocketChannel) key.channel();
        System.out.print(sc.socket().getRemoteSocketAddress() + " ");
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        buffer.clear();
        int len = 0;
        StringBuffer sb = new StringBuffer();
        while ((len = sc.read(buffer)) > 0) {
            buffer.flip();
            sb.append(new String(buffer.array(), 0, len));
        }
        if (sb.length() > 0)
            System.out.println(" "+sb.toString());
        if (sb.toString().trim().toLowerCase().equals("quit")) {
            sc.write(ByteBuffer.wrap("BYE".getBytes()));
            System.out.println("client is closed "
                    + sc.socket().getRemoteSocketAddress());
            key.cancel();
            sc.close();
            sc.socket().close();

        } else {
            String response = userNames.get(sc.socket().getRemoteSocketAddress()) + " said:"
                    + sb.toString();
            System.out.println(response);

            writeMsg = response;

            Iterator<SelectionKey> iterator = key.selector().keys().iterator();

            while (iterator.hasNext()) {
                SelectionKey next = iterator.next();
                if (next != key && next != selectionKey) {
                    if (next.attachment() != null) {
                        next.attach(response);
                    } else {
                        next.attach(response);
                    }
                    next.interestOps(next.interestOps() | SelectionKey.OP_WRITE);
                }

            }
            selector.wakeup();

        }

    }

    public void run() {
        initializeServer();
        execute();
    }

    private void writeMessage(SelectionKey key) throws IOException {
        SocketChannel sc = (SocketChannel) key.channel();
        String str = (String) key.attachment();
        sc.write(ByteBuffer.wrap(str.getBytes()));
        key.interestOps(SelectionKey.OP_READ);
    }
}

