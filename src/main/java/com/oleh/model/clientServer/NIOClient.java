package com.oleh.model.clientServer;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.logging.Level;
import java.util.logging.Logger;

public class NIOClient implements Runnable {

    boolean running;

    SocketChannel socketChannel;

    public NIOClient() {
        running = true;

    }

    public void init() {
        try {
            socketChannel = SocketChannel.open();
            socketChannel.configureBlocking(false);

            socketChannel.connect(new InetSocketAddress("localhost", 8800));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void main(String[] args) {

        NIOClient client = new NIOClient();
        new Thread(client).start();
    }

    public void execute() {
        int num = 0;
        try {
            while (!socketChannel.finishConnect()) {
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        Reader reader = new Reader();
        new Thread(reader).start();

        while (running) {
            try {

                ByteBuffer buffer = ByteBuffer.allocate(1024);
                buffer.clear();

                StringBuffer sb = new StringBuffer();
                Thread.sleep(500);

                while ((num = socketChannel.read(buffer)) > 0) {
                    sb.append(new String(buffer.array(), 0, num));
                    buffer.clear();
                }
                if (sb.length() > 0) {
                    System.out.println(sb.toString());
                }
                if (sb.toString().toLowerCase().trim().equals("quit")) {
                    System.out.println("end connection");
                    socketChannel.close();
                    socketChannel.socket().close();
                    reader.close();
                    running = false;
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(NIOClient.class.getName()).log(
                        Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(NIOClient.class.getName()).log(
                        Level.SEVERE, null, ex);
            }
        }

    }

    public void run() {
        init();
        execute();
    }

    class Reader implements Runnable {

        boolean readerRunning = true;

        public void close() {
            readerRunning = false;
        }

        public void run() {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            while (readerRunning) {
                try {

                    String str = reader.readLine();
                    socketChannel.write(ByteBuffer.wrap(str.getBytes()));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }

        }

    }
}