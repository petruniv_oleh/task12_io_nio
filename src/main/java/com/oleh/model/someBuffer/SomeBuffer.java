package com.oleh.model.someBuffer;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashSet;
import java.util.Set;

public class SomeBuffer {

    public String readFileFromChanel(String fileName) {
        StringBuilder stringBuilder = new StringBuilder();
        try (RandomAccessFile randomAccessFile = new RandomAccessFile(fileName, "r")) {
            FileChannel fileChannel = randomAccessFile.getChannel();
            ByteBuffer buffer = ByteBuffer.allocate(1024);
            Charset charset = Charset.forName("UTF-8");

            while (fileChannel.read(buffer) > 0) {
                buffer.rewind();
                stringBuilder.append(charset.decode(buffer));
                buffer.flip();
            }
            fileChannel.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }

    public void writeFileToChanel(String data, String fileName){
        ByteBuffer byteBuffer = ByteBuffer.wrap(data.getBytes());

        Set options = new HashSet();
        options.add(StandardOpenOption.CREATE);
        options.add(StandardOpenOption.APPEND);

        Path file = Paths.get(fileName);

        try {
            SeekableByteChannel seekableByteChannel = Files.newByteChannel(file, options);
            seekableByteChannel.write(byteBuffer);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
