package com.oleh.view;

import com.oleh.controller.Controller;
import com.oleh.model.directoryInfo.DirectoryInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class View implements ViewInt {

    Controller controller;
    private Map<String, String> menuMap;
    private Map<String, Printable> menuMapMethods;
    private static Logger logger = LogManager.getLogger(View.class.getName());
    Scanner scanner = new Scanner(System.in);

    public View() {
        controller = new Controller();
        menuMap();
        showMenu();
    }

    public void menuMap() {
        menuMap = new LinkedHashMap<>();
        menuMapMethods = new LinkedHashMap<>();
        menuMap.put("1", "1. Serialize");
        menuMap.put("2", "2. Deserialize");
        menuMap.put("3", "3. Test normal reader");
        menuMap.put("4", "4. Test buffered reader");
        menuMap.put("5", "5. Read comments");
        menuMap.put("6", "6. Directory info");
        menuMap.put("7", "7. Read from file");
        menuMap.put("8", "8. Write to file");

        menuMapMethods.put("1", this::serialize);
        menuMapMethods.put("2", this::deserealize);
        menuMapMethods.put("3", this::testNormalReader);
        menuMapMethods.put("4", this::testBufferedReader);
        menuMapMethods.put("5", this::readComments);
        menuMapMethods.put("6", this::directoryInfo);
        menuMapMethods.put("7", this::readFromFile);
        menuMapMethods.put("8", this::writeToFile);

    }

    private void mapMenuOut() {
        logger.info("Menu out");
        System.out.println("\nMenu:");
        for (String s : menuMap.values()
        ) {
            System.out.println(s);
        }
    }

    @Override
    public void showMenu() {
        String key;
        do {
            mapMenuOut();
            System.out.println("====================");
            System.out.println("Select option:");
            key = scanner.nextLine().toUpperCase();
            logger.info("The key for a menu is " + key);
            try {
                menuMapMethods.get(key).print();
            } catch (Exception e) {
                logger.error("Some problem with the key");
            }
        } while (!key.equals("Q"));
    }

    @Override
    public void serialize() {
        logger.info("Invoke serialize");
        controller.serialize();
    }

    @Override
    public void deserealize() {
        logger.info("Invoke deserialize");
        controller.deserialize();
    }

    @Override
    public void testNormalReader() {
        logger.info("Invoke testNormalReader");
        controller.testNormalInputStream();
    }

    @Override
    public void testBufferedReader() {
        logger.info("Invoke testBufferedReader");
        controller.testBufferedInputStream();
    }

    @Override
    public void readComments() {
        logger.info("Invoke readComments");
        System.out.println("Input file name: ");
        String line = scanner.nextLine();
        controller.readComments(line);
    }

    @Override
    public void directoryInfo() {
        logger.info("Invoke wotk with directory");
        System.out.println("Enter directory path: ");
        String path = scanner.nextLine();
        DirectoryInfo directoryInfo = new DirectoryInfo(path);
        printList(directoryInfo.getList());
        String line = scanner.nextLine();
        while (!line.equals("q")) {
            String command = line.substring(0, 3);
            if (command.equals("dir")) {
                String substring1 = line.substring(4, line.length());
                directoryInfo.dir(substring1);
                printList(directoryInfo.getList());
            }
            if (line.equals("cd")) {
                directoryInfo.cd();
                printList(directoryInfo.getList());
            }
            line = scanner.nextLine();
        }

    }

    private void printList(List<String> list) {
        for (String s : list
        ) {
            System.out.println(s);
        }
        System.out.println("Write dir + directory name to go to some directory; \n Write cd to go back " +
                "\n Write q to end work with directories");
    }

    @Override
    public void writeToFile() {
        logger.info("Invoke write to file");
        System.out.println("Enter the name of future file: ");
        String file = scanner.nextLine();
        System.out.println("Enter the data that should be written to file");
        String data = scanner.nextLine();
        controller.writeToFileWithNIO(data, file);
        System.out.println("OK");
    }

    @Override
    public void readFromFile() {
        logger.info("Invoke read from file");
        System.out.println("Enter the name of file to read: ");
        String fileName = scanner.nextLine();
        System.out.println(controller.readFromFileWithNIO(fileName));
    }
}
