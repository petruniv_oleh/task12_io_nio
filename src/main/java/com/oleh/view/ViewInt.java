package com.oleh.view;

public interface ViewInt {
    void showMenu();
    void serialize();
    void deserealize();
    void testNormalReader();
    void testBufferedReader();
    void readComments();
    void directoryInfo();
    void writeToFile();
    void readFromFile();
}
